#!/bin/bash
# Author: xdimedrolx

function install_composer {
    while :; do echo 
        read -p "${CQUESTION}[Php] Do you want to install Composer?: [y/N]${CEND} " -n 1 -r
        if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
            echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
        elif [[ $REPLY =~ ^[Yy]$ ]]; then
            if type composer >/dev/null 2>&1; then
                echo -e "\n${CWARNING}[Php]Composer already installed!${CEND}"
            else 
                curl -sS https://getcomposer.org/installer | php
                mv composer.phar /usr/local/bin/composer   
                # TODO: add permission
                if type composer >/dev/null 2>&1; then
                    echo -e "${CSUCCESS}[Php]Composer install successfully! ${CEND}"
                else 
                    echo -e "${CFAILURE}[Php]Composer install failed! ${CEND}" 
                fi     
            fi     
            break
        elif [[ $REPLY =~ ^[Nn]$ ]]; then
            break
        fi    
    done 
}

function configure_php_fpm {
    service nginx stop
    service php5-fpm stop
    
    php_conf_ini="/etc/php5/fpm/php.ini"
    php_fpm_conf_ini="/etc/php5/fpm/pool.d/www.conf"
    
    #sed -i 's/^display_errors = On/display_errors = Off/' $phpini
    #sed -i 's/^session.cookie_httponly =/session.cookie_httponly = 1/' $phpini
    sed -i 's/^memory_limit = 128M/memory_limit = 1024M/' $php_conf_ini
    #sed -i 's/^register_globals = On/register_globals = Off/' $php_conf_ini
    #sed -i 's/^;date.timezone =/date.timezone = UTC/' $phpini
    #sed -i 's/^session.name = PHPSESSID/session.name = SESSID/' $phpini
    sed -i 's/^;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' $php_conf_ini
    sed -i 's@;error_log = syslog@error_log = /var/log/php/error.log@' $php_conf_ini
    #sed -i 's@;session.save_path =.*@session.save_path = /var/lib/php/session@' $phpini
    
     # Limit FPM processes
    sed -i 's/^pm.max_children.*/pm.max_children = '${FPM_MAX_CHILDREN}'/' $php_fpm_conf_ini
    sed -i 's/^pm.start_servers.*/pm.start_servers = '${FPM_START_SERVERS}'/' $php_fpm_conf_ini
    sed -i 's/^pm.min_spare_servers.*/pm.min_spare_servers = '${FPM_MIN_SPARE_SERVERS}'/' $php_fpm_conf_ini
    sed -i 's/^pm.max_spare_servers.*/pm.max_spare_servers = '${FPM_MAX_SPARE_SERVERS}'/' $php_fpm_conf_ini
    sed -i 's/\;pm.max_requests.*/pm.max_requests = '${FPM_MAX_REQUESTS}'/' $php_fpm_conf_ini
    # Change to socket connection for better performance
    sed -i 's/^listen =.*/listen = \/var\/run\/php5-fpm-www-data.sock/' $php_fpm_conf_ini
}

function install_php {
    apt-get install -y php5-fpm php5 # without apache
    apt-get install -y php5-intl
    apt-get install -y php5-redis
    
    if type nginx >/dev/null 2>&1; then
       echo -e "${CSUCCESS}Php install successfully! ${CEND}"
       
       configure_php_fpm
       install_composer
           
    else 
        echo -e "${CFAILURE}Php install failed! ${CEND}" 
    fi     
}