#!/bin/bash
# Author: xdimedrolx

function configure_beanstalkd {
  sed -i "s/#START=yes/START=yes/g" /etc/default/beanstalkd  
}

function install_beanstalkd {
    apt-get install -y beanstalkd
    
    #if type git >/dev/null 2>&1; then
       configure_beanstalkd
       echo -e "${CSUCCESS}Beanstalkd install successfully! ${CEND}"
    #else 
    #    echo -e "${CFAILURE}Beanstalkd install failed! ${CEND}" 
    #fi     
}
