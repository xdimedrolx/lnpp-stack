#!/bin/bash
# Author: xdimedrolx

echo 'Checking permissions...'
echo
if [[ $EUID -ne 0 ]]; then
    echo "${CFAILURE}This script must be run with root privileges.${CEND}" 1>&2
    echo 'Exiting...'
    exit 1
fi