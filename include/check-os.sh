#!/bin/bash
# Author: xdimedrolx

if [ ! -x '/usr/bin/lsb_release' ]; then
    echo 'You do not appear to be running Ubuntu.'
    echo 'Exiting...'
    exit 1
fi

echo "$(lsb_release -a)"
echo
dis="$(lsb_release -is)"
rel="$(lsb_release -rs)"

if [[ "${dis}" != "Ubuntu" ]]; then
    echo "${dis}: You do not appear to be running Ubuntu"
    echo 'Exiting...'
    exit 1
elif [[ ! "${rel}" =~ ("14.04"|"15.04"|"15.10") ]]; then 
    echo "${CFAILURE}${rel}:${CEND} You do not appear to be running a supported Ubuntu release."
    echo 'Exiting...'
    exit 1
fi