#!/bin/bash
# Author: xdimedrolx
# Todo: added default config

function install_nginx {
    apt-get install -y nginx
    
    if type nginx >/dev/null 2>&1; then
       echo -e "${CSUCCESS}Nginx install successfully! ${CEND}"
    else 
        echo -e "${CFAILURE}Nginx install failed! ${CEND}" 
    fi     
}