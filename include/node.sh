#!/bin/bash
# Author: xdimedrolx

function install_bower {
    while :; do echo 
        read -p "${CQUESTION}[Node] Do you want to install Bower?: [y/N]${CEND} " -n 1 -r
        if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
            echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
        elif [[ $REPLY =~ ^[Yy]$ ]]; then
            if type bower >/dev/null 2>&1; then
                echo -e "\n${CWARNING}[Node]Bower already installed!${CEND}"
            else 
                npm install -g bower   
                if type bower >/dev/null 2>&1; then
                    echo -e "${CSUCCESS}[Node]Bower  install successfully! ${CEND}"
                else 
                    echo -e "${CFAILURE}[Node]Bower  install failed! ${CEND}" 
                fi     
            fi     
            break
        elif [[ $REPLY =~ ^[Nn]$ ]]; then
            break
        fi    
    done 
}

function install_node {
    apt-get install -y nodejs
    apt-get install -y npm
    
    if type nodejs >/dev/null 2>&1; then
        echo -e "${CSUCCESS}Node install successfully! ${CEND}"
        install_bower
    else 
        echo -e "${CFAILURE}Node install failed! ${CEND}" 
    fi     
}