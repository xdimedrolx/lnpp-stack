#!/bin/bash
# Author: xdimedrolx

function install_git {
    apt-get install -y git
    
    if type git >/dev/null 2>&1; then
       echo -e "${CSUCCESS}Git install successfully! ${CEND}"
    else 
        echo -e "${CFAILURE}Git install failed! ${CEND}" 
    fi     
}