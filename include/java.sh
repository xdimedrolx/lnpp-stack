#!/bin/bash
# Author: xdimedrolx

function install_java {
    apt-get install -y oracle-java8-installer
    
    if type java >/dev/null 2>&1; then
        echo -e "${CSUCCESS}Java install successfully! ${CEND}"
    else 
        echo -e "${CFAILURE}Java install failed! ${CEND}" 
    fi     
}