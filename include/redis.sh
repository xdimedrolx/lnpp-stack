#!/bin/bash
# Author: xdimedrolx

#function configure_redis {
#    
#}

function install_redis {
    apt-get install -y redis-server
    
    if type redis-cli >/dev/null 2>&1; then
        echo -e "${CSUCCESS}Redis install successfully! ${CEND}"
        #configure_redis
    else 
        echo -e "${CFAILURE}Redis install failed! ${CEND}" 
    fi     
}