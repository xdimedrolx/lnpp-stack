#!/bin/bash
# Author: xdimedrolx

function configure_sphinx {
  sed -i "s/START=no/START=yes/g" /etc/default/sphinxsearch  
}

function install_sphinx {
    apt-get install -y sphinxsearch
    
    if type indexer >/dev/null 2>&1; then
       configure_sphinx
       echo -e "${CSUCCESS}Sphinx install successfully! ${CEND}"
    else 
        echo -e "${CFAILURE}Sphinx install failed! ${CEND}" 
    fi     
}