#!/bin/bash
# Author: xdimedrolx

function install_postgresql {
    apt-get install -y postgresql-9.4
    apt-get install -y postgresql-contrib-9.4
    
    if type psql >/dev/null 2>&1; then
        echo -e "${CSUCCESS}Postgresql 9.4 install successfully! ${CEND}"
    else 
        echo -e "${CFAILURE}postgresql install failed! ${CEND}" 
    fi     
}