#!/bin/bash
# Author: xdimedrolx

VERSION="1.0.0"

. ./include/color.sh

clear
echo "
####################################################
#     Ananas stack v${VERSION} for Ubuntu 15+      #  
#                                                  #    
####################################################
"

read -p "${CQUESTION}Do you want to continue?[y/N]${CEND} " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then 
    echo -e '\nExiting...'
    exit 0
fi 

echo

. ./include/check-os.sh
. ./include/check-root.sh
source ./options.conf

echo -e "\n${CGREEN}Package updates...${CEND}"
apt-get install -y software-properties-common python-software-properties 
add-apt-repository -y ppa:nginx/stable
add-apt-repository -y ppa:ondrej/php5-5.6
add-apt-repository -y ppa:webupd8team/java
add-apt-repository -y ppa:chris-lea/redis-server
add-apt-repository -y ppa:builds/sphinxsearch-rel22
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
apt-get update
apt-get install -y build-essential

while :; do echo 
    read -p "${CQUESTION}Do you want to install Git?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type git >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Git already installed!${CEND}"
        else 
            . ./include/git.sh
            install_git
        fi
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done

while :; do echo 
    read -p "${CQUESTION}Do you want to install Nginx?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type nginx >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Nginx already installed!${CEND}"
        else 
            . ./include/nginx.sh
            install_nginx
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done    

while :; do echo 
    read -p "${CQUESTION}Do you want to install Php 5.6?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type php >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Php already installed!${CEND}"
        else 
            . ./include/php.sh
            install_php
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done 

while :; do echo 
    read -p "${CQUESTION}Do you want to install java 8?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type java >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Java already installed!${CEND}"
        else 
            . ./include/java.sh 
            install_java
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done

while :; do echo 
    read -p "${CQUESTION}Do you want to install Node.js?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type nodejs >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Node.js already installed!${CEND}"
        else 
            . ./include/node.sh 
            install_node
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done

while :; do echo 
    read -p "${CQUESTION}Do you want to install Postgresql?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type psql >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Postgresql already installed!${CEND}"
        else 
            . ./include/postgresql.sh 
            install_postgresql
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done 

while :; do echo 
    read -p "${CQUESTION}Do you want to install Redis?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type redis-cli >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Redis already installed!${CEND}"
        else 
            . ./include/redis.sh 
            install_redis
        fi
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi    
done

while :; do echo 
    read -p "${CQUESTION}Do you want to install Beanstalkd?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        #if type redis >/dev/null 2>&1; then
        #    echo -e "\n${CWARNING}Beanstalkd already installed!${CEND}"
        #else 
            . ./include/beanstalkd.sh 
            install_beanstalkd
        #fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi
done

while :; do echo 
    read -p "${CQUESTION}Do you want to install Sphinx?: [y/N]${CEND} " -n 1 -r
    if [[ ! $REPLY =~ ^[NnYy]$ ]]; then
        echo -e "\n${CWARNING}input error! Please only input 'y' or 'n'${CEND}"
    elif [[ $REPLY =~ ^[Yy]$ ]]; then
        if type indexer >/dev/null 2>&1; then
            echo -e "\n${CWARNING}Sphinx already installed!${CEND}"
        else 
            . ./include/sphinx.sh 
            install_sphinx
        fi     
        break
    elif [[ $REPLY =~ ^[Nn]$ ]]; then
        break
    fi
done

echo -e "\n${CGREEN}Stack installation complete${CEND}\n"